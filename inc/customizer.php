<?php
/**
 * codebusta Theme Customizer.
 *
 * @package codebusta
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function codebusta_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'codebusta_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function codebusta_customize_preview_js() {
	wp_enqueue_script( 'codebusta_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'codebusta_customize_preview_js' );

function codebusta_customizer( $wp_customize ) {

  /**
   * Change existing controls and settings
   */
  
	$wp_customize->get_control( 'blogname' )->priority         = '1';
	$wp_customize->get_control( 'blogdescription' )->priority         = '2';
	$wp_customize->get_control( 'display_header_text' )->priority         = '3';
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	
	/**
	 * Change existing sections
	 */
	
	$wp_customize->get_section( 'header_image' )->panel = 'header';
	$wp_customize->get_section( 'title_tagline' )->panel = 'header';
 	$wp_customize->get_section( 'header_image' )->priority = '1';
	$wp_customize->get_section( 'title_tagline' )->priority = '2';
	$wp_customize->get_section( 'colors' )->panel = 'base';
	$wp_customize->get_section( 'colors' )->priority = '1';
	$wp_customize->get_section( 'background_image' )->panel = 'base';
	$wp_customize->get_section( 'background_image' )->priority = '2';
	$wp_customize->get_section( 'static_front_page' )->panel = 'base';
	$wp_customize->get_section( 'static_front_page' )->priority = '3';
 
  /**
   * Add panels
   */
    
  $wp_customize->add_panel( 'base', array( 'title' => __('Base Settings', 'artist_showcase'), 'priority' => '1' ) );
	$wp_customize->add_panel( 'header', array( 'title' => __('Header', 'codebusta'), 'priority' => '2' ) );
	
	
}
add_action( 'customize_register', 'codebusta_customizer' );
