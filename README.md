Codebusta WP Theme
===

Hi. I'm a starter theme called `Codebusta`. I'm a theme meant for hacking with Zurb Foundation already built in, with a lot of extra features in the Customizer.

Getting Started
---------------


Now you're ready to go! The next step is easy to say, but harder to do: make an awesome WordPress theme. :)

Good luck!